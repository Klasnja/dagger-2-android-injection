package selena.com.base.injection

import dagger.Module
import dagger.android.ContributesAndroidInjector
import selena.com.base.presentation.detail.DetailActivity
import selena.com.base.presentation.detail.DetailActivityModule
import selena.com.base.presentation.detail.DetailFragmentProvider
import selena.com.base.presentation.main.MainActivity
import selena.com.base.presentation.main.MainActivityModule


@Module
interface ActivityBinder {

    @ContributesAndroidInjector(modules = arrayOf(MainActivityModule::class))
    fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector(modules = arrayOf(DetailActivityModule::class, DetailFragmentProvider::class))
    fun bindDetailActivity(): DetailActivity
}
