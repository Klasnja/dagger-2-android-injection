package selena.com.base.injection

import android.app.Application
import android.content.Context

import dagger.Binds
import dagger.Module

@Module
interface AppModule {

    @Binds
    fun provideContext(application: Application): Context

}
