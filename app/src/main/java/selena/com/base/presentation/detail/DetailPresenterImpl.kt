package selena.com.base.presentation.detail

import selena.com.base.data.ApiService
import selena.com.base.presentation.common.BasePresenter
import javax.inject.Inject

class DetailPresenterImpl @Inject constructor(private var detailView: DetailView, private var apiService: ApiService) : BasePresenter {

    fun loadDetail() {
        apiService.loadData()
        detailView.onDetailLoaded()
    }
}
