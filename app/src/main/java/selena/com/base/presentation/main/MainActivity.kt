package selena.com.base.presentation.main

import android.content.Intent
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*
import selena.com.base.R
import selena.com.base.presentation.common.BaseActivity
import selena.com.base.presentation.detail.DetailActivity

class MainActivity : BaseActivity<MainPresenterImpl>(), MainView {

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter.loadMain()

        button.setOnClickListener {
            startActivity(Intent(this, DetailActivity::class.java))
        }
    }

    override fun onMainLoaded() {
        Log.v("TEST", "Main page is loaded.")
    }
}
