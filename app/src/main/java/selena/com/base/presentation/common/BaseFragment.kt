package selena.com.base.presentation.common

import dagger.android.support.DaggerFragment
import javax.inject.Inject


abstract class BaseFragment<PresenterType: BasePresenter> : DaggerFragment() {

    @Inject
    lateinit var presenter: PresenterType
}