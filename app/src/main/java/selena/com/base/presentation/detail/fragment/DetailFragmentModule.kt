package selena.com.base.presentation.detail.fragment

import dagger.Binds
import dagger.Module


@Module
interface DetailFragmentModule {

    @Binds
    fun provideDetailFragmentView(detailFragment: DetailFragment): DetailFragmentView
}
