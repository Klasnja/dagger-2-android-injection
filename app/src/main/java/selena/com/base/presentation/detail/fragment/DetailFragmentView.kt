package selena.com.base.presentation.detail.fragment

interface DetailFragmentView {

    fun onDetailFragmentLoaded()
}
