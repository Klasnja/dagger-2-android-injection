package selena.com.base.presentation.main

import dagger.Binds
import dagger.Module


@Module
interface MainActivityModule {

    @Binds
    fun provideMainView(mainActivity: MainActivity): MainView


    fun provideMainPresenter(): MainPresenterImpl

}
