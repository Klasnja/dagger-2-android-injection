package selena.com.base.presentation.detail

interface DetailView {
    fun onDetailLoaded()
}
