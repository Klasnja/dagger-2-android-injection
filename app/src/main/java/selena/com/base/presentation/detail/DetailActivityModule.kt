package selena.com.base.presentation.detail

import dagger.Binds
import dagger.Module


@Module
interface DetailActivityModule {

    @Binds
    fun provideDetailView(detailActivity: DetailActivity): DetailView

    fun provideDetailPresenter(): DetailPresenterImpl
}
