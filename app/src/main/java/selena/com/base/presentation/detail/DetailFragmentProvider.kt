package selena.com.base.presentation.detail

import dagger.Module
import dagger.android.ContributesAndroidInjector
import selena.com.base.presentation.detail.fragment.DetailFragment
import selena.com.base.presentation.detail.fragment.DetailFragmentModule


@Module
interface DetailFragmentProvider {

    @ContributesAndroidInjector(modules = arrayOf(DetailFragmentModule::class))
    fun provideDetailFragmentFactory(): DetailFragment
}
