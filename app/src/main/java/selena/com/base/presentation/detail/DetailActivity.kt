package selena.com.base.presentation.detail

import android.os.Bundle
import android.util.Log
import selena.com.base.R
import selena.com.base.presentation.common.BaseActivity
import selena.com.base.presentation.detail.fragment.DetailFragment


class DetailActivity : BaseActivity<DetailPresenterImpl>(), DetailView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        presenter.loadDetail()

        if (savedInstanceState == null)
            supportFragmentManager
                    .beginTransaction()
                    .add(R.id.container, DetailFragment.newInstance())
                    .commitAllowingStateLoss()
    }

    override fun onDetailLoaded() {
        Log.v("TEST", "Detail is loaded")
    }

}
