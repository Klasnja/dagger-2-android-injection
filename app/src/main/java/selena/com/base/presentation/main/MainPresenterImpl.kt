package selena.com.base.presentation.main

import selena.com.base.data.ApiService
import selena.com.base.presentation.common.BasePresenter
import javax.inject.Inject

class MainPresenterImpl @Inject constructor(private var mainView: MainView, private var apiService: ApiService) : BasePresenter {

    fun loadMain() {
        apiService.loadData()
        mainView.onMainLoaded()
    }
}
