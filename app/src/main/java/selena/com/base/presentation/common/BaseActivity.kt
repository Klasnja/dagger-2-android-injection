package selena.com.base.presentation.common

import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject


abstract class BaseActivity<PresenterType: BasePresenter> : DaggerAppCompatActivity() {

    @Inject
    lateinit var presenter: PresenterType
}