package selena.com.base.presentation.detail.fragment

import selena.com.base.presentation.common.BasePresenter
import javax.inject.Inject


class DetailFragmentPresenter @Inject constructor(private var detailFragmentView: DetailFragmentView) : BasePresenter {

    init {
        detailFragmentView.onDetailFragmentLoaded()
    }
}
