package selena.com.base

import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import selena.com.base.injection.DaggerAppComponent


class App : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        val appComponent = DaggerAppComponent.builder().application(this).build()
        appComponent.inject(this)
        return appComponent
    }
}
